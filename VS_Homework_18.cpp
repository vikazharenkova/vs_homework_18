﻿// VS_Homework_18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>

template <class T> // это шаблон класса с T вместо фактического (передаваемого) типа данных
class Stack
{
public:
    // Конструктор пустого стека, длина которого задается пользователем 
    Stack(int _length)
    {
        length = _length;
        stack = new T[_length]();
    }


    // Деструктор 
    ~Stack()
    {
        delete[] stack;
    }
    // Обнуление
    void Erase()
    {
        delete[] stack;
        // Присваиваем значение nullptr для m_data, чтобы на выходе не получить висячий указатель!
        stack = nullptr;
        length = 0;
    }



    //Вывод в консоль стека
    void ShowStack()
     {
        for (int i = 0; i <= length-1; i++)
        {
            std::cout << stack[i] << ' ';
        }
     }

    //Добавление элемента на вершину стека
    void Push()
    {
        T x;
        std::cout << "Introduce a new stack element: ";
        std::cin >> x;

        for (int i = 0; i <= length - 1; i++)
        {
            if (stack[i] != 0 and i == length - 1)
            {
                std::cout << "Stack is full!" << std::endl;
                break;
            }
            else if (stack[i] == 0)
            {
                stack[i] = x;
                break;
            }
         }
    }
        
    // Удаление элемента с вершины стека и его возвращение
    void Pop()
    {
        for (int i = 0; i <= length - 1; i++)
        {
            if (stack[i] == 0 and i == 0)
            {
                std::cout << "Stack is empty!" << std::endl;
                break;
            }
            else if (stack[i] == 0 and i != 0)
            {
                T y = stack[i - 1];
                std::cout << "Removed stack element equal to " << y << std::endl;
                stack[i-1] = 0;
                break;
            }
            else if (stack[i] != 0 and i == length - 1)
            {
                T y = stack[i];
                std::cout << "Removed stack element equal to " << y << std::endl;
                stack[i] = 0;
                break;
            }
         }
    }

    // Возвращаем верхний элемент стека, но не удаляем его
    void Peek()
    {
        for (int i = 0; i <= length - 1; i++)
        {
            if (stack[i] == 0 and i == 0)
            {
                std::cout << "Stack is empty!" << std::endl;
                break;
            }
            else if (stack[i] == 0 and i != 0)
            {
                std::cout << "The top of the stack is " << stack[i - 1] << std::endl;
                break;
            }
            else if (stack[i] != 0 and i == length - 1)
            {
                std::cout << "The top of the stack is " << stack[i] << std::endl;
                break;
            }
        }
    }

    // Возвращаем количество элементов в стеке
    void Count()
    {
        for (int i = 0; i <= length - 1; i++)
        {
            if (stack[i] == 0)
            {
                std::cout << "There are " << i << " elements on the stack" << std::endl; // номер элемента стека равен индексу массива, увеличенного на 1
                break;
            }
            else if (stack[i] != 0 and i == length - 1)
            {
                std::cout << "There are " << length << " elements on the stack"  << std::endl;; // номер элемента стека равен индексу массива, увеличенного на 1
                break;
            }
        }
    }

private:
    int length;
    T* stack;
};

int main()
{
    int StackSize;
    std::cout << "Enter the stack size: ";
    std::cin >> StackSize;
    Stack<double> FirstStack(StackSize);
    FirstStack.ShowStack(); 
    std::cout << std::endl;

    for (int i = 0; i < 5; i++)
    {
        FirstStack.Push();
        FirstStack.ShowStack();
        std::cout << std::endl;
    }

    for (int i = 0; i < 3; i++)
    {
        FirstStack.Pop();
        FirstStack.ShowStack();
        std::cout << std::endl;
    }

    FirstStack.Push();
    FirstStack.ShowStack();
    std::cout << std::endl;

    FirstStack.Peek();
    FirstStack.ShowStack();
    std::cout << std::endl;

    FirstStack.Count();
    FirstStack.ShowStack();
    std::cout << std::endl;

    return 0;
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
